using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class ButtonController : MonoBehaviour
{
    public InputField inputField;
    public InputField inputField1;
    public Bezier_Viz bezierCurve;

    public void ButtonPressed()
    {
        string inputText = inputField.text;
        // ������������ ������ � �����
        if (int.TryParse(inputText, out int numberOfPoints))
        {
            // ��������� ��������� � ����� � ����
            GenerateAndWriteCoordinates(numberOfPoints);
        }
        else
        {
            Debug.Log("�������: ���������� �������� ������� �����");
            return;
        }
    }

    public void BernstainPressed()
    {
        string inputText = inputField1.text;
        // ������������ ������ � �����
        if (!float.TryParse(inputText, NumberStyles.Float, CultureInfo.InvariantCulture, out float t))
        {
            Debug.Log("�������: ���������� �������� t");
            return;
        }
        else if (t > 1 || t < 0)
        {
            Debug.Log("�������: t ������� ���� �� 0 �� 1");
            return;
        }

        string filePath = "D:\\lpnu\\labs4term\\CG\\LAB2\\My project\\bernstain.txt";

        using (StreamWriter writer = new StreamWriter(filePath))
        {
            for (int i = 0; i < bezierCurve.controlPoints.Count - 3; i++)
            {
                float basis = Bernstein(bezierCurve.controlPoints.Count - 1, i, t);
                writer.WriteLine($"b({bezierCurve.controlPoints.Count - 1},{i}): {basis}");
            }
        }

        Debug.Log("������� ���������� �������� � ����");
    }

    private void GenerateAndWriteCoordinates(int numberOfPoints)
    {
        if (bezierCurve.curve.Count == 0)
        {
            Debug.Log("�������: ����� ����� �� ����������");
            return;
        }

        string filePath = "D:\\lpnu\\labs4term\\CG\\LAB2\\My project\\coordinates.txt";

        using (StreamWriter writer = new StreamWriter(filePath))
        {
            for (int i = 0; i < numberOfPoints; i++)
            {
                float x = bezierCurve.curve[i].x;
                float y = bezierCurve.curve[i].y;

                // ����� ��������� � ����
                writer.WriteLine($"����� {i + 1}: ({x}, {y})");
            }
        }

        Debug.Log("���������� �������� � ����");
    }

    private static float[] Factorial = new float[]
    {
        1.0f,
        1.0f,
        2.0f,
        6.0f,
        24.0f,
        120.0f,
        720.0f,
        5040.0f,
        40320.0f,
        362880.0f,
        3628800.0f,
        39916800.0f,
        479001600.0f,
        6227020800.0f,
        87178291200.0f,
        1307674368000.0f,
        20922789888000.0f,
    };

    private static float Binomial(int n, int i)
    {
        float ni;
        float a1 = Factorial[n];
        float a2 = Factorial[i];
        float a3 = Factorial[n - i];
        ni = a1 / (a2 * a3);
        return ni;
    }

    private static float Bernstein(int n, int i, float t)
    {
        float t_i = Mathf.Pow(t, i);
        float t_n_minus_i = Mathf.Pow((1 - t), (n - i));

        float basis = Binomial(n, i) * t_i * t_n_minus_i;

        return basis;
    }

    public void ParameterPressed()
    {
        Bezier_Viz.curveMethod = 1;
    }

    public void RecursionPressed()
    {
        Bezier_Viz.curveMethod = 2;
    }
}
