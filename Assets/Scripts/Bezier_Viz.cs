using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bezier_Viz : MonoBehaviour
{
    [HideInInspector]
    public List<Vector2> curve;
    public static int curveMethod = 0;

    public List<Vector2> controlPoints = new List<Vector2>()
    {
        new Vector2(-5.0f, -5.0f),
        new Vector2( 0.0f,  2.0f),
        new Vector2( 5.0f, -2.0f)
    };

    // Prefab ��� ���������� �����
    public GameObject PointPrefab;

    LineRenderer[] mLineRenderers = null;

    List<GameObject> mPointGameObjects = new List<GameObject>();

    // ���������� ����
    public float LineWidth;
    public float LineWidthBezier;
    public Color LineColor = new Color(0.5f, 0.5f, 0.5f, 0.8f);
    public Color BezierCurveColor = new Color(0.5f, 0.6f, 0.8f, 0.8f);

    // �������� Line Renderer
    private LineRenderer CreateLine()
    {
        GameObject obj = new GameObject();
        LineRenderer lr = obj.AddComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Sprites/Default"));
        lr.startColor = LineColor;
        lr.endColor = LineColor;
        lr.startWidth = LineWidth;
        lr.endWidth = LineWidth;
        lr.sortingOrder = 6;
        return lr;
    }

    // Start is called before the first frame update
    void Start()
    {
        mLineRenderers = new LineRenderer[3];
        mLineRenderers[0] = CreateLine();
        mLineRenderers[1] = CreateLine();
        mLineRenderers[2] = CreateLine();

        mLineRenderers[0].gameObject.name = "LineRenderer_obj_0";
        mLineRenderers[1].gameObject.name = "LineRenderer_obj_1";
        mLineRenderers[2].gameObject.name = "LineRenderer_obj_2";

        for (int i = 0; i < controlPoints.Count; i++)
        {
            GameObject obj = Instantiate(PointPrefab, controlPoints[i], Quaternion.identity);
            obj.name = "ControlPoint_" + i.ToString();
            mPointGameObjects.Add(obj);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (curveMethod != 0)
        {
            LineRenderer lineRenderer1 = mLineRenderers[0];
            LineRenderer lineRenderer2 = mLineRenderers[1];
            LineRenderer curveRenderer = mLineRenderers[2];

            List<Vector2> pts = new List<Vector2>();
            for (int i = 0; i < mPointGameObjects.Count; i++)
            {
                pts.Add(mPointGameObjects[i].transform.position);
            }

            //�������
            // lineRenderer.positionCount = pts.Count;
            // for (int i = 0; i < pts.Count; i++)
            // {
            lineRenderer1.SetPosition(0, pts[0]);
            lineRenderer1.SetPosition(1, pts[1]);

            lineRenderer2.SetPosition(0, pts[pts.Count - 2]);
            lineRenderer2.SetPosition(1, pts[pts.Count - 1]);

            // }

            if (curveMethod == 1)
            {
                curve = BezierCurve.PointList2Parameter(pts, 0.01f);
            }
            if (curveMethod == 2)
            {
                curve = BezierCurve.PointList2(pts, 0.01f);
            }
            curveRenderer.startColor = BezierCurveColor;
            curveRenderer.endColor = BezierCurveColor;
            curveRenderer.positionCount = curve.Count;
            curveRenderer.startWidth = LineWidthBezier;
            curveRenderer.endWidth = LineWidthBezier;

            for (int i = 0; i < curve.Count; i++)
            {
                curveRenderer.SetPosition(i, curve[i]);
            }
        }
    }

    //������ ���������� ����� ��� ��������� ����������
    private void OnGUI()
    {
        Event e = Event.current;
        if (e.isMouse)
        {
            if (e.clickCount == 2 && e.button == 0)
            {
                Vector2 rayPos = new Vector2(
                  Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
                  Camera.main.ScreenToWorldPoint(Input.mousePosition).y);

                InsertNewControlPoint(rayPos);
                controlPoints.Add(rayPos);
            }
        }
    }

    void InsertNewControlPoint(Vector2 p)
    {
        if (mPointGameObjects.Count >= 18)
        {
            Debug.Log("Cannot create any more control points. Max is 18");
            return;
        }

        GameObject obj = Instantiate(PointPrefab, p, Quaternion.identity);
        obj.name = "ControlPoint_" + mPointGameObjects.Count.ToString();
        mPointGameObjects.Add(obj);
    }
}
